[![Language - Javascript](https://img.shields.io/badge/Javascript-F2DB3F?style=for-the-badge&logo=javascript&logoColor=white)](https://)
[![Approch - BDD](https://img.shields.io/badge/Cucumber-green?style=for-the-badge&logo=cucumber&logoColor=white)](https://)



<br/>
<br/>
<div align="center">
    <img src="./logo.png" alt="Logo" width="10%">
    <br/>
    <br/>
    <h1 align="center"><strong>Formation Cypress - BDD Cucumber</strong></h1>
    <br />
    <p align="center">Docker - Page Object - Actions - CI - Visual Regression</p>
</div>
  
 
<br/>
<br/>
